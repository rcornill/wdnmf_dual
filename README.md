# WDNMF_dual : Wasserstein Dictionary-based Nonnegative Matrix Factorization using the dual problem

This repository contains codes, tests and scripts for reproducing the wdnmf_dual package.

It can be easily installed by running from this repo
pip install . -r requirements.txt

Web documentation for the package will be available under doc/build/html/index.html

This package could (probably) be used to reproduce experiences in an incoming paper

Please cite the incoming paper if you use this package.

Based on an article by Antoine Rolet, Marco Cuturi and Gabriel Peyré article "Fast Dictionary Learning with a Smoothed Wasserstein Loss", see here :  https://proceedings.mlr.press/v51/rolet16.html for more info. 
