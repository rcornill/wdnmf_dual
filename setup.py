#!/usr/bin/env python

import setuptools

setuptools.setup(
      name='wdnmf_dual',
      version='0.0',
      description='Code for Dictionary-based NMF with Wasserstein loss',
      author='Remi Cornillet',
      author_email='remi.cornillet@hotmail.fr.fr',
      packages=setuptools.find_packages()
     )

