# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 10:32:48 2022

@author: remic
"""


import wdnmf_dual.scripts.data_generation as dt 
import torch as tn
from wdnmf_dual.methods.base_function import *
from wdnmf_dual.methods.grad_pb_dual import *
from wdnmf_dual.methods.Optim_W import optim_w, optim_pb_dual_w, solW_primal
from wdnmf_dual.methods.Optim_H import optim_h
from wdnmf_dual.methods.solver import solver_wdnmf
from wdnmf_dual.methods.base_function import compute_matK, simplex_norm
import matplotlib.pyplot as plt
import plotly.express as px
from plotly.offline import iplot

n = 8 #Nb d'atome dans le dictionnaire
dim = 25 #Nb de pixel dans l'image
size = 100 #Longueur des atomes
oversize = 1 #Les atomes pouvant subir red/blue shift, la taille des atomes artificielles est grandie puis tronquée.
mean_spread = 10 #Largeur moyenne des pics
sp_spread = 3 #Ecart type de la largeur
mean_prob = 0.7 #Probabilité moyenne d'avoir une valeur non nulle à une place précise dans le pic
sp_prob = 0.2 #Ecart type de la probabilité moyenne de valeur non nulle
mean_noise = 0.3 #Bruit moyen des valeurs du pic
sp_noise = 0.05 #Ecart type du bruit
gb_noise = 5e-5 #Valeur du bruit gaussien sur l'ensemble de la longueur 
shift = 1
snr = 25


tn.manual_seed(1)

r = 4
sigma = [0, 1, 2, 3]


dico = dt.make_dico(n, size, oversize,
    mean_spread, sp_spread,
    mean_prob, sp_prob,
    mean_noise, sp_noise,
    gb_noise)

w_init = dico[:, sigma]

cost = tn.zeros((size, size), dtype=tn.float64)
for i in range(n):
    for j in range(n):
        cost[i, j] = (i-j)**2

data, h_init, gaussian_noise = dt.shift_data(w_init, oversize, shift, dim, cost = cost, snr = snr)

data = data/sum(data)

dico = dico[oversize:(size+oversize), :]

reg = 1e0
ent = 1e0
rho1 = 1e0
rho2 = 1e0
itertotal = 4
step_g1 = 5e-4
step_g2 = 5e-4
step_h = 5e-3
iterdual_w = 1000
iterdual_h = 100


"""(out_w, out_h, out_sigma, _) = solver_wdnmf(data, dico, reg, ent, rho1, rho2,
                                         cost = cost, r = r, itertotal = itertotal, 
                                         step_g1 = step_g1, step_g2 = step_g2, step_h = step_h,
                                         jump= 1,
                                         iterdual_w=iterdual_w, iterdual_h=iterdual_h)"""


iter_g1 = 10
iter_g2 = 10

g1 = tn.rand((size, dim), dtype=tn.float64)
g1 = simplex_norm(g1)

g2 = tn.rand((size, r), dtype=tn.float64)
g2 = simplex_norm(g2)
matK = compute_matK(cost, ent) #That is tn.exp(-cost/ent), just a short cup

(a, b) = optim_pb_dual_w(g1, g2, rho1, h_init, data, sigma, dico, reg, ent,
                     matK, step_g1, step_g2, iter_g1, iter_g2, iterdual_w)

w_optimized = solW_primal(a, b, h_init, rho1)

fig3 = px.line(tn.cat((w_optimized, dico), dim=1 ), title = "Dictionary and optimized templates", range_y=[0, 1], labels=[])
fig3.data[0].name = "Optimized template 1"
fig3.data[1].name = "Optimized template 2"
fig3.data[2].name = "Optimized template 3"
fig3.data[3].name = "Optimized template 4"
fig3.data[4].name = "Dictionary template 1"
fig3.data[5].name = "Dictionary template 2"
fig3.data[6].name = "Dictionary template 3"
fig3.data[7].name = "Dictionary template 4"
fig3.show()