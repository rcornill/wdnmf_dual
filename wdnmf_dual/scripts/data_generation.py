# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 10:32:48 2022

@author: remic
"""


import torch as tn
import random as rd
import numpy as np
import ot
import scipy.stats as ss
from wdnmf_dual.methods.base_function import simplex_norm

def snr_to_epsilon(signal, noise, snr):
    epsilon = 10**(-snr/20) * tn.linalg.norm(signal) / tn.linalg.norm(noise)
    return epsilon

def spike(pos, i, noise):
    n = 2*(rd.random()-0.5)*noise
    if pos == i:
        return 1+2*n
    else :
        return (1+n)/(abs(pos-i))

def make_atom(size, spread = 4, prob=0.4, noise=0.2, oversize=0, gb_noise=1e-5):
    size_total = size + 2*oversize
    atom = tn.rand((size_total, 1), dtype=tn.float64)*gb_noise
    pos = rd.randint(oversize, size_total-oversize)
    for i in range(max(0, pos-spread), min(size_total-1, pos+spread)):
        if i == pos:
            atom[i, 0] = spike(pos, i, noise)
        else:
            if (rd.random()<prob):
                atom[i, 0] = spike(pos, i, noise)
    return atom


def make_dico(size_dic, dim, oversize, 
              mean_spread, sp_spread,
              mean_prob, sp_prob, 
              mean_noise, sp_noise, gb_noise=1e-5): 
    gt_os = tn.zeros((dim + 2 * oversize, size_dic), dtype=tn.float64)

    for i in range(size_dic):
        atom_temp = make_atom(size = dim, 
                       spread = rd.randint(mean_spread - sp_spread, mean_spread + sp_spread), 
                       prob =  max(0.05, rd.normalvariate(mean_prob, sp_prob)),
                       noise = max(0.05, rd.normalvariate(mean_noise, sp_noise)), 
                       oversize = oversize, 
                       gb_noise = gb_noise)
        gt_os[:, i] = atom_temp[:,0]/atom_temp[:,0].sum()
    return gt_os


def clean_data(gt, m = 100): #gt must be normalized
    (dim, rank) = gt.size()
    h_init = np.random.rand(rank, m)
    h_init = h_init/sum(h_init)
    img = np.dot(gt, h_init)
    data = tn.tensor(img, dtype=tn.float64)    
    return data, gt, h_init

def snr_data(gt, snr, m = 100):
    (dim, rank) = gt.size()
    img, _, h_init = clean_data(gt, m)
    noise = tn.randn((dim, m), dtype=tn.float64)
    gaussian_noise = snr_to_epsilon(img, noise, snr) * noise
    data = (img + gaussian_noise)
    return data, gt, h_init

def clean_data_os(gt, oversize, m = 100): #gt must be normalized
    (dim_os, rank) = gt.size()
    dim = dim_os - 2*oversize
    h_init = np.random.rand(rank, m)
    h_init = h_init/sum(h_init)
    img = np.dot(gt[oversize : dim+oversize,:], h_init)
    data = tn.tensor(img, dtype=tn.float64)    
    return data, gt, h_init

def snr_data_os(gt, snr, oversize, m = 100):
    (dim_os, rank) = gt.size()
    dim = dim_os - 2*oversize
    img, _, h_init = clean_data(gt, oversize, m)
    noise = tn.randn((dim, m), dtype=tn.float64)
    gaussian_noise = snr_to_epsilon(img, noise, snr) * noise
    data = (img + gaussian_noise)
    return data, gt, h_init

def shift_data_old(gt, shift, m=100, cost = None):
    (dim, rank) = gt.size()
    if cost is None :
        cost = tn.zeros(dim, dim)
        for i in range(dim):
            for j in range(dim):
                cost[i, j] = (i-j)**2
    
    cible_f = tn.zeros((dim, 1), dtype=tn.float64)
    cible_e = tn.zeros((dim, 1), dtype=tn.float64)  
    cible_f[0] = 1
    cible_e[dim-1]=1
    
    c_f = np.array(cible_f[:,0])
    c_e = np.array(cible_e[:,0])
    
    h_init = np.random.rand(rank, m, dtype="float64")
    h_init = h_init/sum(h_init)
    h_init = tn.tensor(h_init, dtype=tn.float64)
    
    data = tn.zeros((dim, m), dtype=tn.float64)
    
    for i in range(m):
        gt_temp = tn.zeros((dim, rank))
        for j in range(rank):
            if (rd.random()-0.5)>0:
                atom = np.asarray(gt[:,j], dtype="float64")
                rd_shift = shift*rd.random()
                gt_temp[:,j]= ot.barycenter(np.stack((atom, c_e)).T, M=cost, reg=0.5, 
                                             weights=np.array([1-rd_shift, rd_shift]), numItermax=25000, method="sinkhorn_log")
            else:
                atom = np.asarray(gt[:,j], dtype="float64")
                rd_shift = shift*rd.random()
                gt_temp[:,j]= ot.barycenter(np.stack((atom, c_f)).T, M=cost, reg=0.5, 
                                             weights=np.array([1-rd_shift, rd_shift]), numItermax=25000, method="sinkhorn_log")
            
        data[:,i] = tn.matmul(gt_temp, h_init[:,i])
    return data

def get_prob(sh):
    x = np.arange(-sh, sh+1)
    xU, xL = x + 0.5, x - 0.5 
    prob = ss.norm.cdf(xU, scale = np.sqrt(sh)) - ss.norm.cdf(xL, scale = np.sqrt(sh))
    prob = prob / prob.sum() 
    return (x, prob)

def shift_data(gt_os, oversize=0, shift=0, m=100, cost = None, snr=None):
    
    sh = min(oversize, shift)
    (x, prob) = get_prob(sh)
    
    (dim_OS, rank) = gt_os.size()
    
    dim = dim_OS-2*oversize
    
    if cost is None :
        cost = tn.zeros(dim, dim)
        for i in range(dim):
            for j in range(dim):
                cost[i, j] = (i-j)**2
    
    h_init = np.random.rand(rank, m)
    h_init = h_init/sum(h_init)
    h_init = tn.tensor(h_init, dtype=tn.float64)
    
    data = tn.zeros((dim, m), dtype=tn.float64)
    
    for i in range(m):
        gt_temp = tn.zeros((dim, rank), dtype=tn.float64)
        for j in range(rank):
            pos_init = oversize + np.random.choice(x, size = 1, p = prob)[0]
            gt_temp[:,j] = gt_os[pos_init : pos_init + dim, j]
        data[:,i] = tn.matmul(gt_temp, h_init[:,i])
    
    if snr is None : 
        return data
    else : 
        noise = abs(tn.randn((dim, m), dtype=tn.float64))
        gaussian_noise = snr_to_epsilon(data, noise, snr) * noise
        data = data + gaussian_noise
        return data, h_init, gaussian_noise