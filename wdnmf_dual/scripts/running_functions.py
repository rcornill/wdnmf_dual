import wdnmf_dual as wd
import torch as tn
from wdnmf_dual.methods.grad_pb_dual import pb_dual, grad1_pb_dual, pb
from wdnmf_dual.methods.base_function import compute_matK, simplex_norm
from wdnmf_dual.methods.Optim_W import optim_w
from wdnmf_dual.methods.Optim_H import optim_h

n = 3
m = 4
r = 2
rho1 = 0.1

sigma = [0,1]
reg = 0.1
ent = 0.1
rho2 = 0.1

cost = tn.zeros((n, n), dtype=tn.float64)
for i in range(n):
    for j in range(n):
        cost[i, j] = (i-j)**2

matK = compute_matK(cost, ent)
"""
for i in range(5):
    #aim = tn.ones(n, m) + tn.rand(n,m)
    #aim = simplex_norm(aim)
    
    #dictionary = tn.tensor([[0.05, 0.05],[0.9, 0.05], [0.05, 0.9]], dtype=tn.float64)
    dictionary = tn.abs(tn.rand(n,2, dtype=tn.float64))
    dictionary = simplex_norm(dictionary)
    
    #w = tn.tensor([[0.9, 0.05],[0.05, 0.05], [0.05, 0.9]], dtype=tn.float64)
    w = tn.ones(n, r) + tn.rand(n,r)
    w = simplex_norm(w)
    
    h = tn.ones(r, m) + tn.rand(r,m)
    h = simplex_norm(h)
    
    data = dictionary@h
    

    
    
    # out = pb_dual(aim, w, rho1, h, data, sigma, dictionary, reg, ent, matK)
    
    # out2 = grad1_pb_dual(aim, w, rho1, h, data, ent, matK)
    
    #print(w)
    
    #print(dictionary)
    
    
    out3 = optim_w(w, h, data, sigma, dictionary, reg, ent, rho1, rho2, cost,
                   stepg1=1e-3, stepg2=1e-3, iterdual = 500, itermax = 1, numItermax= 1000)
    
    #print(out3)
    
    out4 = pb(w = out3, h = h, sigma = sigma, data = data, dictionary = dictionary, reg = reg, 
         ent = ent, rho1 = rho1, rho2=rho2, cost = cost)
    
    print("Coût après optim : " + str(float(out4)))
    print("Coût optimal : " + str(float(pb(w = dictionary, h = h, sigma = sigma, data = data, dictionary = dictionary, reg = reg, 
         ent = ent, rho1 = rho1, rho2=rho2, cost = cost))))
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")"""

#dictionary = tn.tensor([[0.05, 0.05],[0.9, 0.05], [0.05, 0.9]], dtype=tn.float64)
dictionary = tn.abs(tn.rand(n,2, dtype=tn.float64))
dictionary = simplex_norm(dictionary)

#w = tn.tensor([[0.9, 0.05],[0.05, 0.05], [0.05, 0.9]], dtype=tn.float64)
h_init = simplex_norm(tn.rand(r,m))

h = simplex_norm(tn.rand(r,m))
    
data = dictionary@h

out5 = optim_h(dictionary, h_init, data, sigma, dictionary, reg, ent, rho1, rho2, cost, 
               step_w = 1e-2, iterdual = 1000, itermax = 1,
               numItermax = 1000, thr = 1e-9, verbose = False)

print(out5)
print(h)
print(h_init)