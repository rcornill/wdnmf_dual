# -*- coding: utf-8 -*-
"""
Created on Tue Jan 17 13:57:40 2023

@author: rcornill
"""

import pytest
import numpy as np
import torch as tn
from wdnmf_dual.methods.base_function import *

class TestClass:
    def test_log_mat1():
        x = tn.zeros(2, 3, dtype=tn.float64)
        v = tn.ones(2, 3, dtype=tn.float64)
        assert tn.all(x == log_mat(v))
    
    def test_log_mat2():
        x = tn.ones(2, 3, dtype=tn.float64)
        v = tn.zeros(2, 3, dtype=tn.float64)
        x[1, 1] = 2
        v[1, 1] = np.log(2)
        # changed x to v
        assert tn.all(v == log_mat(x))
    
    def test_simplex_prox_mat_projection():
        x = tn.ones(2, 3, dtype=tn.float64)
        x = x/sum(x)
        assert tn.all(x == simplex_prox_mat(x))
        
    def test_simplex_prox_mat1(): # La projection se fait selon l2 et non l1 ici!
        x = tn.tensor([[1, 2, 0], [3, 2, 1]], dtype=tn.float64)
        w = tn.tensor([[0., 0.5, 0], [1, 0.5, 1]], dtype=tn.float64)
        assert tn.all(w == simplex_prox_mat(x))
        
    def test_simplex_norm_projection():
        x = tn.ones(2, 3, dtype=tn.float64)
        x = x/sum(x)
        assert tn.all(x == simplex_norm(x))
        
    def test_simplex_norm1(): # La projection se fait selon l1 ici!
        x = tn.tensor([[1, 2, 0], [3, 2, 1]], dtype=tn.float64)
        w = tn.tensor([[0.25, 0.5, 0], [0.75, 0.5, 1]], dtype=tn.float64)
        assert tn.all(w == simplex_norm(x))
    
    def test_e_vec():
        x = tn.tensor([0.5, 1, 0.1], dtype=tn.float64)
        w = 0.5*np.log(0.5) + 0 + 0.1*np.log(0.1)
        assert tn.all(e_vec(x)==w)
    
    def test_e_mat():
        x = tn.tensor([[0.5, 1, 0.1]], dtype=tn.float64)
        w = 0.5*np.log(0.5) + 0 + 0.1*np.log(0.1)
        assert tn.all(e_mat(x)==w)
    
    def test_e_dual():
        x = tn.tensor([1, 0, 0.5], dtype=tn.float64)
        w = -np.log(np.e + 1 + np.exp(0.5))
        assert tn.all(e_dual(x)==w)
    
    