# -*- coding: utf-8 -*-
"""
Created on Tue Jan 25 19:43:38 2022

@author: remic
"""

import ot as ot
import torch as tn
import numpy as np


def simplex_prox_mat(v, z=1):
    """
    For a given matrix V seen as a collection of vectors, return the matrix W
    with all vectors being the L2 projection of V's on the simplex. 
    Warning : it does promote sparsity in each vector due to L2 projection, 
    this may be problematic within the code to use that if not needed.

    Parameters
    ----------
    v : 2D tn.tensor
        collection of vectors to project on the simplex
    z : Float, optional
        The columns of v are normalized to z. The default is 1.

    Returns
    -------
    w : 2D tn.tensor
        V with each vectors projected on the simplex.

    """
    # Tu veux que ca marche en colonne pour la matrice v j'imagine?
    # je propose de modifier comme ca, c'est pareil mais plus lisible je trouve
    # TODO: POT is slow... consider using self-make implem
    n, m = v.shape
    w = tn.zeros(n, m, dtype=tn.float64)
    for i in range(m):
        w[:, i] = ot.utils.proj_simplex(v[:,i], z)
    return w

def simplex_norm(v) :
    """
    For a given matric seen as a collectin of vectors, return the columnwise normalized matrix.
    Considering the matrix is nonnegative, the returned matrix is on the simplex.
    The relu asset that their is no negative value within V. 
    Parameters
    ----------
    v : 2D tn.tensor
        Collection of nonnegative vectors

    Returns
    -------
    w : 2D tn.tensor
        The columnwise normalized version of the matrix v

    """
    vprime = tn.nn.functional.relu(v, inplace=False)
    n, m = v.shape
    w = tn.zeros(n, m, dtype = tn.float64)
    if not(tn.all(vprime == v)) :
        print("\nWarning : The entry vector had a negative value. It has been removed but it may be an issue somehow, somewhen, somewhere. \n")
    # Pourquoi faire ca? si on normalise on normalise, et puis c'est tout. Tu peux laisser le print mais normalise v pas vprime
    for i in range(m):
        w[:, i] = vprime[:,i] / sum(vprime[:,i])
    return w


def e_mat(v):
    """
    Non-negativity constraint, compute the Frobenius inner product of v with log(v), v being a matrix
    the log been calculated elementwise. This function is concave

    Parameters
    ----------
    v : 2D tn.tensor
        Matrix with every column within the simplex (nonnegative, sum to one)

    Returns
    -------
    temp : float
        Result of \langle v, \log(v) \rangle

    """
    return tn.dot(v.flatten(),tn.nan_to_num(tn.log(v), nan=0).flatten())

def e_dual(v):
    """
    Dual of the e function.

    Parameters
    ----------
    v : 1 or 2D tn.tensor
        Vector within the simplex

    Returns
    -------
    float
        The value of the dual of e for the given vector v        

    """
    return - float(sum(tn.logsumexp(v, 0)))

def grad_e_dual(v):
    """
    Return -softmax(v), or -\frac{exp(v[i])}{\Sigma_j(exp([j]))}

    Parameters
    ----------
    v : 1 or 2D tn.tensor
        Vector within the simplex

    Returns
    -------
    w : 1 or 2D tn.tensor
        softmax(v)

    """
    return - tn.softmax(v,0)

def f_vec(source, aim, cost, ent, method = 'sinkhorn', numItermax = 1000, thr = 1e-9, verbose = False):
    """
    Return the entropic Wasserstein distance between the "source" vector and the "aim" vector, 
    with respect to the ground cost "cost". The entropic regulation is T \rightarrow \langle T, \log T \rangle
    See POT (pythonot.github.io) for more info, this is basically just the 
    sinkhorn algorithm from this package.

    Parameters
    ----------
    source : 1D tn.tensor
        Source vector, need to be on the simplex
    aim : 1D tn.tensor
        Aimed vector, need to be on the simplex
    cost : 2D tn.tensor
        Ground cost, need to match source and aim dimension
    ent : float
        Entropic regulation rate. The closer to 0, the sharper the transport is
    method : string,  optional
        ‘sinkhorn’,’sinkhorn_log’, ‘greenkhorn’, ‘sinkhorn_stabilized’ or ‘sinkhorn_epsilon_scaling’
        depending on what solver to use. Sinkhorn stabilized should be the best for
        sharper transport with the smaller reg rate. The default is 'sinkhorn'.
    numItermax : int, optional
        Number of iteration before stoping the calculation of the transport plan. The default is 1000.
    thr : float, optional
        Precision threshold, the calculation of the transport plan stop if the \delta between
        two iterations of the computation is smaller than thr. The default is 1e-9.
    verbose : Boolean, optional
        Verbose or not verbose, this is a debug question. The default is False.

    Returns
    -------
    temp : float
        Distance between the source and aim vector

    """
    temp = ot.bregman.sinkhorn2(a = source, b = aim, M = cost, reg = ent, method = method, 
                                numItermax = numItermax, stopThr = thr, verbose = verbose, 
                                log = False, warn = False)
    return temp
    
def f_mat(source, aim, cost, ent, method = 'sinkhorn', numItermax = 1000, thr = 1e-9, verbose = False):
    """
    Return the entropic Wasserstein distance between the "source"'s vector and the "aim"'s vector, 
    with respect to the ground cost "cost". The entropic regulation is T \rightarrow \langle T, \log T \rangle
    See POT (pythonot.github.io) for more info, this is basically just the 
    sinkhorn algorithm from this package.

    Parameters
    ----------
    source : 2D tn.tensor
        Source matrix, need to be on the simplex columnwise
    aim : 2D tn.tensor
        Aimed matrix, need to be on the simplex columnwise
    cost : 2D tn.tensor
        Ground cost, need to match source and aim dimension
    ent : float
        Entropic regulation rate. The closer to 0, the sharper the transport is
    method : string,  optional
        ‘sinkhorn’,’sinkhorn_log’, ‘greenkhorn’, ‘sinkhorn_stabilized’ or ‘sinkhorn_epsilon_scaling’
        depending on what solver to use. Sinkhorn stabilized should be the best for
        sharper transport with the smaller reg rate. The default is 'sinkhorn'.
    numItermax : int, optional
        Number of iteration before stoping the calculation of the transport plan. The default is 1000.
    thr : float, optional
        Precision threshold, the calculation of the transport plan stop if the \delta between
        two iterations of the computation is smaller than thr. The default is 1e-9.
    verbose : Boolean, optional
        Verbose or not verbose, this is a debug question. The default is False.

    Returns
    -------
    temp : float
        Sum of the distances between the source's and aim's vectors

    """
    # TODO: merge with vec version? annoying to have two different functions for mat and vec.
    temp = 0
    n, m = source.shape
    for j in range(m):
        temp += ot.bregman.sinkhorn2(a = source[:,j], b = aim[:,j], M = cost, reg = ent, method = method, 
                                    numItermax = numItermax, stopThr = thr, verbose = verbose, 
                                    log = False, warn = False)
    return temp

def f_vec_aux(aim, cost, ent, method = 'sinkhorn', numItermax = 1000, thr = 1e-9, verbose = False):
    """
    Return an easier function to manipulate than f_vec
    Parameters
    ----------
    aim : 1D tn.tensor
        Aimed vector, need to be on the simplex
    cost : 2D tn.tensor
        Ground cost, need to match source and aim dimension
    ent : float
        Entropic regulation rate. The closer to 0, the sharper the transport is
    method : string,  optional
        ‘sinkhorn’,’sinkhorn_log’, ‘greenkhorn’, ‘sinkhorn_stabilized’ or ‘sinkhorn_epsilon_scaling’
        depending on what solver to use. Sinkhorn stabilized should be the best for
        sharper transport with the smaller reg rate. The default is 'sinkhorn'.
    numItermax : int, optional
        Number of iteration before stoping the calculation of the transport plan. The default is 1000.
    thr : float, optional
        Precision threshold, the calculation of the transport plan stop if the \delta between
        two iterations of the computation is smaller than thr. The default is 1e-9.
    verbose : Boolean, optional
        Verbose or not verbose, this is a debug question. The default is False.

    Returns
    -------
    f : function
        Same than f_mat, but you only have to add the source.

    """
    def f(source):
        return f_vec(source, aim, cost, ent, method = 'sinkhorn', numItermax = 1000, thr = 1e-9, verbose = False)
    return f

def f_mat_aux(aim, cost, ent, method = 'sinkhorn', numItermax = 1000, thr = 1e-9, verbose = False):
    """
    Return an easier function to manipulate than f_mat
    Parameters
    ----------
    aim : 2D tn.tensor
        Aimed matrix, need to be on the simplex columnwise
    cost : 2D tn.tensor
        Ground cost, need to match source and aim dimension
    ent : float
        Entropic regulation rate. The closer to 0, the sharper the transport is
    method : string,  optional
        ‘sinkhorn’,’sinkhorn_log’, ‘greenkhorn’, ‘sinkhorn_stabilized’ or ‘sinkhorn_epsilon_scaling’
        depending on what solver to use. Sinkhorn stabilized should be the best for
        sharper transport with the smaller reg rate. The default is 'sinkhorn'.
    numItermax : int, optional
        Number of iteration before stoping the calculation of the transport plan. The default is 1000.
    thr : float, optional
        Precision threshold, the calculation of the transport plan stop if the \delta between
        two iterations of the computation is smaller than thr. The default is 1e-9.
    verbose : Boolean, optional
        Verbose or not verbose, this is a debug question. The default is False.

    Returns
    -------
    f : function
        Same than f_mat, but you only have to add the source.

    """
    def f(source):
        return f_mat(source, aim, cost, ent, method = 'sinkhorn', numItermax = 1000, thr = 1e-9, verbose = False)
    return f

def compute_matK(cost, ent):
    """
    Return the matrix K to shorten the calculus. This matrix is $ e^{-Cost/ent}$

    Parameters
    ----------
    cost : 2d tensor
        Cost matrix
    ent : float
        Entropic regulation rate.

    Returns
    -------
    w : 2d tensor
        $K = e^{-Cost/ent}$

    """
    return tn.exp(-cost/ent)
    #n, m = cost.shape
    #w = tn.zeros(n, m, dtype=tn.float64)
    #for i in range(n):
        #for j in range(m):
            #w[i, j] = np.exp(-cost[n, m]/ent)
    #return w
    

def f_dual(g, aim, ent, matK):
    """
    Compute the dual of $f$. 

    Parameters
    ----------
    g : TYPE
        DESCRIPTION.
    aim : TYPE
        DESCRIPTION.
    ent : TYPE
        DESCRIPTION.
    matK : TYPE
        DESCRIPTION.

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    matKalpha = tn.log(tn.matmul(matK, tn.exp(g/ent)))
    return ent*(e_mat(aim) + tn.sum(aim*matKalpha))

def grad_f_dual(g, aim, ent, matK):
    """
    Compute the gradient of $f^*$ for a given aim vector. Need the computation 
    of matK to limit the calculation time.

    Parameters
    ----------
    g : 1D tn.tensor
        Vector within the simplex, this is the dual variable
    aim : 1D tn.tensor
        Aimed vector
    ent : float64
        Used to compute $\alpha = \exp^{g / ent}$
    matK : 2D tn.tensor
        Precalculated matrix, $\matK = \exp^{-\frac{\matM}{ent}}

    Returns
    -------
    b : 1d tn.tensor
        Gradient of $f^*$ 

    """
    matKT = tn.transpose(matK, 0, 1)
    alpha = tn.exp(g/ent)
    a = matKT@(aim/tn.matmul(matK, alpha))
    return alpha*a
    #return b
