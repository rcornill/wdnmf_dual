"""
Created on Fri Jan 13 14:43:03 2023

@author: rcornill
"""

import torch as tn
from wdnmf_dual.methods.base_function import *
from wdnmf_dual.methods.grad_pb_dual import *

def optim_pb_dual_w(g1, g2, rho1, h, data, sigma, dictionary, reg, ent, matK,
                  stepg1, stepg2, iterg1, iterg2, iterdual = 10):
    """
    

    Parameters
    ----------
    g1 : 2d tn.tensor
        First variable of the dual pb, size n*m
    g2 : 2d tn.tensor
        Second variable of the dual pb, size n*r
    rho1 : float64
        Regulation rate for the nonnegativity constraint w.r.t W
    h : 2d tn.tensor
        Abundance matrix, size r*m
    data : 2d tn.tensor
        Data entry, of size n*m
    sigma : 1d array-like
        Array of size r, corresponding to the r selected template of the dictionary
    dictionary : 2d tn.tensor
        Dictionary matrix, each columns is a different template, size n*d with d>r
    reg : float64
        Dictionary-based regulation rate
    ent : float64
        Entropic regulation rate
    matK : 2d tn.tensor
        Precalculated matrix, $\matK = \exp^{-\frac{\matM}{reg}}
    stepg1 : float
        Step size for gradient descent for g1
    stepg2 : float
        Step size for gradient descent for g2
    iterg1 : int 
        Number of iteration to update g1 during iternal loop
    iterg2 : int
        Number of iteration to update g2 during iternal loop
    iterdual : int, optional
        Number of total loop to update g1 and g2 before updating W. The default is 10.

    Returns
    -------
    g1 : 2d tn.tensor
        First variable of the dual pb, size n*m, updated
    g2 : 2d tn.tensor
        Second variable of the dual pb, size n*r, updated

    """
    
    n1, m1 = g1.shape
    n2, r = g2.shape
    
    rep_g1 = tn.clone(g1).detach()
    rep_g2 = tn.clone(g2).detach()
    
    for _ in range(iterdual):
        
        for _ in range(iterg1):
            g1_grad = grad1_pb_dual(rep_g1, rep_g2, rho1, h, data, ent, matK)
            rep_g1 = rep_g1 + stepg1*g1_grad 
            if tn.any(rep_g1 < 0):
                rep_g1 = simplex_prox_mat(rep_g1)
        
        for _ in range(iterg2):
            g2_grad = grad2_pb_dual(rep_g1, rep_g2, rho1, h, sigma, dictionary, reg, ent, matK)
            rep_g2 = rep_g2 + stepg2*g2_grad
            if tn.any(rep_g2 < 0):
                rep_g2 = simplex_prox_mat(rep_g2)
    
        
    return (rep_g1, rep_g2)

def solW_primal(g1, g2, h, rho1):
    w = -grad_e_dual(-(tn.matmul(g1, tn.transpose(h, 0, 1)) + g2 )/rho1)
    return w

def optim_w(w_init, h, data, sigma, dictionary, reg, ent, rho1, rho2, cost,
            g1_init = None, g2_init = None,
            stepg1 = 2e-3, stepg2 = 2e-3, iterg1 = 1, iterg2 = 1, iterdual = 150, 
            method = 'sinkhorn', numItermax = 500, 
            thr = 1e-9, verbose = False):
    n, r = w_init.shape
    _, m = h.shape
    """ assert n, m = data.shape """
    _, d = dictionary.shape
    
    matK = compute_matK(cost, ent)
    
    if g1_init is None :
        g1 = tn.rand((n, m), dtype=tn.float64) # Peut se remplacer par un g_init
        g1 = simplex_prox_mat(g1)
    else :
        g1 = g1_init
    
    if g2_init is None :
        g2 = tn.rand((n, r), dtype=tn.float64)
        g2 = simplex_prox_mat(g2)
    else :
        g2 = g2_init
    
    g1, g2 = optim_pb_dual_w(g1, g2, rho1, h, data, sigma, dictionary, reg, ent, 
                               matK, stepg1, stepg2, iterg1, iterg2, iterdual)
    w = solW_primal(g1, g2, h, rho1)
    
    return w, g1, g2