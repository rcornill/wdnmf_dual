# -*- coding: utf-8 -*-
"""
Created on Tue Mar 21 15:38:21 2023

@author: rcornill
"""

import torch as tn
import ot
from scipy.optimize import linear_sum_assignment 

def assignment (loss_mat): #, optimal = False):
    _, indice = linear_sum_assignment(loss_mat.detach().numpy())
    return indice


def loss_matrix(w, dico, ent, cost , numItermax = 1000, method = 'sinkhorn',
                thr = 1e-9, verbose = False, log = False, warn=False):
    """
    Compute the distance between each rows of mat1 and rows of mat2.
    Parameters
    ----------
    mat1 : Float tn.tensor
        2d tensor
    mat2 : Float tn.tensor
        2d tensor
    epsilon : float, optional
        A positive float. If it's 0, we are using the usual Lp Wass distance,
        else we're using the entropic Wass distance and we then need a cost.
        The default is 0.
    cost : Float pt.tensor (array-like), optional
        This is the cost matrice for entropic wass distance,
        containing the distance from each point of supp(mat1) and supp(mat2).
        Needed if epsilon != 0.
        The default is None.

    Returns
    -------
    loss_mat :
        Matrice, each point (i, j) is the Wasserstein distance betwen mat1_i and mat2_j.

    """
    m1,r1 = w.shape
    m2,r2 = dico.shape
    loss_mat=tn.ones((r1,r2), dtype=tn.float64)
    
    for i in range(r1):
        for j in range(r2):
            loss_mat[i,j] = ot.bregman.sinkhorn2(a = w[:,i], b = dico[:,j],
                                        M = cost, reg = ent, method = method, 
                                        numItermax = numItermax, stopThr = thr, 
                                        verbose = verbose, 
                                        log = log, warn = warn)
    return loss_mat


def update_sigma(d, w, cost, ent, numItermax = 1000, method = "sinkhorn"):
    loss_mat = loss_matrix(w, d, ent, cost, numItermax=numItermax, method=method)
    sigma = assignment(loss_mat)
    return sigma
