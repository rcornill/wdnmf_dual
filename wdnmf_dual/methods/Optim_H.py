# -*- coding: utf-8 -*-
"""
Created on Wed Mar 15 14:37:33 2023

@author: rcornill
"""

"""
Created on Fri Jan 13 14:43:03 2023

@author: rcornill
"""

import torch as tn
from wdnmf_dual.methods.base_function import *
from wdnmf_dual.methods.grad_pb_dual import *

def optim_pb_dual_h(g, rho2, w, data, sigma, dictionary, ent, matK,
                  step_h, iterdual = 10):
    """
    

    Parameters
    ----------
    g : 2d tn.tensor
        Dual variable, size n*m
    rho2 : float64
        Regulation rate for the nonnegativity constraint w.r.t H
    h : 2d tn.tensor
        Abundance matrix, size r*m
    data : 2d tn.tensor
        Data entry, of size n*m
    sigma : 1d array-like
        Array of size r, corresponding to the r selected template of the dictionary
    dictionary : 2d tn.tensor
        Dictionary matrix, each columns is a different template, size n*d with d>r
    step_w : float
        Step size for gradient descent for g1
    iterdual : int, optional
        Number of total loop to update g1 and g2 before updating W. The default is 10.

    Returns
    -------
    g : 2d tn.tensor
        Dual variable, size n*r, updated

    """
    
    rep_g = tn.clone(g).detach()
    
    for _ in range(iterdual):
        g_grad = gradh_pb_dual(rep_g, rho2, w, data, ent, matK)
        rep_g = rep_g + step_h*g_grad 
        if tn.any(rep_g<0):
            rep_g = simplex_prox_mat(rep_g)
    return rep_g
    
def solH_primal(g, w, rho2):
    h = -grad_e_dual(-tn.matmul(tn.transpose(w, 0, 1), g)/rho2)
    return h

def optim_h(w, h_init, data, sigma, dictionary, reg, ent, rho1, rho2, cost,
            g_init = None,
            step_h = 1e-3, iterdual = 500, method = 'sinkhorn',
            numItermax = 1000, thr = 1e-9, verbose = False):
    n, r = w.shape
    _, m = h_init.shape
    """ assert n, m = data.shape """
    _, d = dictionary.shape
    
    matK = compute_matK(cost, ent)
    if g_init is None:
        g = tn.rand((n, m), dtype=tn.float64) # Peut se remplacer par un g_init
        g = simplex_prox_mat(g)
    else :
        g = g_init
    
    g = optim_pb_dual_h(g, rho2, w, data, sigma, dictionary, ent, matK, 
                          step_h, iterdual)
    h = solH_primal(g, w, rho2)
        
    
    return h, g