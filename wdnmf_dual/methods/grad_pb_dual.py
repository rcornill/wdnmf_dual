# -*- coding: utf-8 -*-
"""
Created on Tue Jan 25 19:43:38 2022

@author: remic
"""

import torch as tn
import wdnmf_dual.methods.base_function as dg

def pb(w, h, sigma, data, dictionary, reg, ent, rho1, rho2, cost,
       method = 'sinkhorn', numItermax = 1000, thr = 1e-9, verbose = False):
    """
    Return the cost of the primal problem

    Parameters
    ----------
    w : 2D tn.tensor
        Template matrix, each column being liked to the matching one in dictionary. Size n \times r
    h : 2D tn.tensor
        Weight matrix, size t \times m
    sigma : np.array
        Matching between each column of W and Dictionary
    data : 2D tn.tensor
        Data, each column is a pixel / a spectrum, size n \times m
    dictionary : 2D tn.tensor
        Each column is a known template, size n \times d with d > r.
    reg : float
        parameter \lambda > 0, the lower it is, the less importance the dictionary have. 
        \lambda = 0 leads to the base case.
    ent : float
        Entropic regulation rate. The lower this rate is, the sharper the 
    rho1 : float
        Regulation to inforce the nonnegativity constraint for W.
    rho2 : float 
        Regulation to inforce the nonnegativity constraint for H.
    cost : 2D tn.tensor
        Distance matrix between each bin.
    method : str, optional
        ‘sinkhorn’,’sinkhorn_log’, ‘greenkhorn’, ‘sinkhorn_stabilized’ or ‘sinkhorn_epsilon_scaling’
        depending on what solver to use. Sinkhorn stabilized should be the best for
        sharper transport with the smaller reg rate. The default is 'sinkhorn'.
    numItermax : int, optional
        Number of iteration before stoping the calculation of the transport plan. The default is 1000.
    thr : float, optional
        Precision threshold, the calculation of the transport plan stop if the \delta between
        two iterations of the computation is smaller than thr. The default is 1e-9.
    verbose : Boolean, optional
        Verbose or not verbose, this is a debug question. The default is False.

    Returns
    -------
    temp : float
        Total cost of the primal problem.

    """
    approx = tn.matmul(w, h)
    temp = dg.f_mat(source = approx, aim = data, cost = cost, ent = ent, 
                  method = method, numItermax= numItermax, thr = thr, 
                  verbose = False)
    temp -= rho1 * dg.e_mat(w) + rho2 * dg.e_mat(h)
    temp += reg * dg.f_mat(source = w, aim = dictionary[:,sigma], cost = cost, ent = ent,
                  method = method, numItermax = numItermax, thr = thr,
                  verbose = False)
    return temp

def pb_dual(g1, g2, rho1, h, data, sigma, dictionary, reg, ent, matK): 
    """
    Compute the dual of the primal pb
    Parameters
    ----------
    g1 : 2d tn.tensor
        First variable of the dual pb, size n*m
    g2 : 2d tn.tensor
        Second variable of the dual pb, size n*r
    rho1 : float64
        Regulation rate for the nonnegativity constraint w.r.t W
    h : 2d tn.tensor
        Abundance matrix, size r*m
    data : 2d tn.tensor
        Data entry, of size n*m
    sigma : 1d array-like
        Array of size r, corresponding to the r selected template of the dictionary
    dictionary : 2d tn.tensor
        Dictionary matrix, each columns is a different template, size n*d with d>r
    reg : float64
        Dictionary-based regulation rate
    ent : float64
        Entropic regulation rate
    matK : 2d tn.tensor
        Precalculated matrix, $\matK = \exp^{-\frac{\matM}{reg}}

    Returns
    -------
    temp : TYPE
        DESCRIPTION.

    """
    _, m1 = g1.shape
    _, m2 = g2.shape
    temp = rho1 * dg.e_dual( - (tn.matmul(g1, tn.transpose(h, 0, 1)) + g2)/rho1 )

    matKT = tn.transpose(matK, 0, 1)
    temp -= dg.f_dual(g1, data, ent, matK)
    
    dic_sigma = dictionary[:,sigma]
    temp -= reg*dg.f_dual(g2/reg, dic_sigma, ent, matK)

    return temp

def grad1_pb_dual(g1, g2, rho1, h, data, ent, matK):
    """
    Compute the gradient w.r.t g1 of the dual pb
    function is f(g1, g2)= \rho_1 E_*((g1@H^T + g2)/\rho_1) -\Sigma_i f^*_{Y_i}(g1_i) - \lambda \Sigma_j f^*_{D_j}(g2_j)
    supposed to be h(g1, g2) = \nabla E_*((g1@H^T + g2)/\rho_1)@H -\Sigma_i f^*_{Y_i}(g1_i)

    Parameters
    ----------
    g1 : 2d tn.tensor
        First variable of the dual pb, size n*m
    g2 : 2d tn.tensor
        Second variable of the dual pb, size n*r
    rho1 : float64
        Regulation rate for the nonnegativity constraint w.r.t W
    h : 2d tn.tensor
        Abundance matrix, size r*m
    data : 2d tn.tensor
        Data entry, of size n*m
    ent : float64
        Entropic regulation rate
    matK : 2d tn.tensor
        Precalculated matrix, $\matK = \exp^{-\frac{\matM}{reg}}
        
    Returns
    -------
    temp : 2d tn.tensor
        Gradient w.r.t g1 of the dual pb

    """
    
    hT = tn.transpose(h,0,1)
    
    e =  dg.grad_e_dual(-(g1@hT + g2)/rho1)@h
    f_data = dg.grad_f_dual(g1, data, ent, matK)

    return -e-f_data

def grad2_pb_dual(g1, g2, rho1, h, sigma, dictionary, reg, ent, matK):
    """
    Compute the gradient w.r.t. g2 of the dual pb

    Parameters
    ----------
    g1 : 2d tn.tensor
        First variable of the dual pb, size n*m
    g2 : 2d tn.tensor
        Second variable of the dual pb, size n*r
    rho1 : float64
        Regulation rate for the nonnegativity constraint w.r.t W
    h : 2d tn.tensor
        Abundance matrix, size r*m
    sigma : 1d array-like
        Array of size r, corresponding to the r selected template of the dictionary
    dictionary : 2d tn.tensor
        Dictionary matrix, each columns is a different template, size n*d with d>r
    reg : float64
        Dictionary-based regulation rate
    ent : float64
        Entropic regulation rate
    matK : 2d tn.tensor
        Precalculated matrix, $\matK = \exp^{-\frac{\matM}{reg}}

    Returns
    -------
    temp : 1d tn.tensor
        Gradient w.r.t g2 of the dual pb

    """
    hT = tn.transpose(h,0,1)
    
    e =  dg.grad_e_dual(-(g1@hT + g2)/rho1)
    dico = dictionary[:,sigma]
    f_dico = dg.grad_f_dual(g2/reg, dico, ent, matK)

    return -e-f_dico

def gradh_pb_dual(g, rho2, w, data, ent, matK):
    wT = tn.transpose(w, 0, 1)
    e = w@dg.grad_e_dual(-wT@g/rho2)
    f = dg.grad_f_dual(g, data, ent, matK)
    return -e-f