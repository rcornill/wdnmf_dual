# -*- coding: utf-8 -*-
"""
Created on Thu Mar 23 10:29:17 2023

@author: rcornill
"""

import torch as tn
from wdnmf_dual.methods.base_function import *
from wdnmf_dual.methods.grad_pb_dual import *
from wdnmf_dual.methods.Optim_W import optim_w
from wdnmf_dual.methods.Optim_H import optim_h
from wdnmf_dual.methods.solver import solver_wdnmf

r = 2 #Rang
n = 8 #Taille des atomes
m = 4 #
d = 4

reg = 1
ent = 0.5
rho1 = 0.5
rho2 = 0.5

dictionary = simplex_norm(tn.rand((n, d), dtype = tn.float64))
sigma = [2, 3]

h = simplex_norm(tn.rand((r, m), dtype = tn.float64))

data = dictionary[:,sigma]@h

cost = tn.zeros((n, n), dtype=tn.float64)
for i in range(n):
    for j in range(n):
        cost[i, j] = (i-j)**2

(out_w, out_h, out_sigma, _) = solver_wdnmf(data, dictionary, reg, ent, rho1, rho2,
                                         cost, r=2, itertotal=10, 
                                         step_g1=5e-3, step_g2=5e-3, step_h=5e-3,
                                         iterdual_w=700, iterdual_h=700)

total_cost = pb(dictionary[:,sigma], h, sigma, data, dictionary, reg, ent, rho1, rho2,
                cost, numItermax=1000)

print("Coût opti : " + str(total_cost) + "\n")
    