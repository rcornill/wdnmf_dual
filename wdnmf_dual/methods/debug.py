# -*- coding: utf-8 -*-
"""
Created on Thu Mar 30 11:26:21 2023

@author: rcornill
"""

import torch as tn
from wdnmf_dual.methods.grad_pb_dual import pb_dual, grad1_pb_dual, pb
from wdnmf_dual.methods.base_function import compute_matK, simplex_norm
from wdnmf_dual.methods.Optim_W import optim_w, optim_pb_dual_w, solW_primal
import plotly.express as px

tn.manual_seed(3)

n = 4
m = 4
r = 2
rho1 = 0.1
rho2 = 0.1

sigma = [0,1]
reg = 0.1
ent = 0.1

cost = tn.zeros((n, n), dtype=tn.float64)
for i in range(n):
    for j in range(n):
        cost[i, j] = (i-j)**2

matK = compute_matK(cost, ent) #That is tn.exp(-cost/ent), just a short cut

dictionary = simplex_norm(tn.rand(n,r, dtype=tn.float64))

w = simplex_norm(tn.rand(n,r, dtype=tn.float64))

h = simplex_norm(tn.rand(r, m, dtype=tn.float64))

data = dictionary@h

out1 = pb(w = w, h = h, sigma = sigma, data = data, dictionary = dictionary, reg = reg, 
         ent = ent, rho1 = rho1, rho2=rho2, cost = cost, numItermax = 5000)

print(out1)

out2 = pb(w = dictionary, h = h, sigma = sigma, data = data, dictionary = dictionary, reg = reg, 
         ent = ent, rho1 = rho1, rho2=rho2, cost = cost, numItermax = 5000)

print(out2)

g1 = tn.rand((n, m), dtype=tn.float64)
g1 = simplex_norm(g1)

g2 = tn.rand((n, r), dtype=tn.float64)
g2 = simplex_norm(g2)

stepg1 = 5e-3
stepg2 = 5e-3
iterg1 = 1
iterg2 = 1
iterdual = 500

(a, b) = optim_pb_dual_w(g1, g2, rho1, h, data, sigma, dictionary, reg, ent,
                     matK, stepg1, stepg2, iterg1, iterg2, iterdual)
print(a)
print(b)