# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 09:54:05 2023

@author: rcornill
"""
import torch as tn
from wdnmf_dual.methods.base_function import *
from wdnmf_dual.methods.grad_pb_dual import *
from wdnmf_dual.methods.Optim_W import optim_w
from wdnmf_dual.methods.Optim_H import optim_h
from wdnmf_dual.methods.update_sigma import update_sigma
import matplotlib.pyplot as plt

def solver_wdnmf(data, dictionary, 
                 reg, ent, rho1, rho2, cost, r,
                 w_init = None, h_init = None, sigma_init = None,
                 step_g1 = 1e-3, step_g2 = 1e-3, iter_g1 = 1, iter_g2 = 1,
                 iterdual_w = 1,
                 step_h = 1e-3, iter_h = 1, iterdual_h = 1,
                 jump = 1,
                 itertotal = 1000,
                 method = 'sinkhorn', numItermax = 1000, thr = 1e-9,
                 verbose = False):
    """
    

    Parameters
    ----------
    data : TYPE
        DESCRIPTION.
    dictionary : TYPE
        DESCRIPTION.
    reg : TYPE
        DESCRIPTION.
    ent : TYPE
        DESCRIPTION.
    rho1 : TYPE
        DESCRIPTION.
    rho2 : TYPE
        DESCRIPTION.
    cost : TYPE
        DESCRIPTION.
    r : TYPE
        DESCRIPTION.
    w_init : TYPE, optional
        DESCRIPTION. The default is None.
    h_init : TYPE, optional
        DESCRIPTION. The default is None.
    sigma_init : TYPE, optional
        DESCRIPTION. The default is None.
    step_g1 : TYPE, optional
        DESCRIPTION. The default is 1e-3.
    step_g2 : TYPE, optional
        DESCRIPTION. The default is 1e-3.
    iter_g1 : TYPE, optional
        DESCRIPTION. The default is 1.
    iter_g2 : TYPE, optional
        DESCRIPTION. The default is 1.
    iterdual_w : TYPE, optional
        DESCRIPTION. The default is 1.
    step_h : TYPE, optional
        DESCRIPTION. The default is 1e-3.
    iter_h : TYPE, optional
        DESCRIPTION. The default is 1.
    iterdual_h : TYPE, optional
        DESCRIPTION. The default is 1.
    itertotal : TYPE, optional
        DESCRIPTION. The default is 1000.
    method : TYPE, optional
        DESCRIPTION. The default is 'sinkhorn'.
    numItermax : TYPE, optional
        DESCRIPTION. The default is 1000.
    thr : TYPE, optional
        DESCRIPTION. The default is 1e-9.
    verbose : TYPE, optional
        DESCRIPTION. The default is False.

    Returns
    -------
    w : TYPE
        DESCRIPTION.
    h : TYPE
        DESCRIPTION.
    sigma : TYPE
        DESCRIPTION.
    cost_list : TYPE
        DESCRIPTION.

    """
    n, m = data.shape
    if w_init is None:
        w = simplex_norm(tn.rand((n, r), dtype=tn.float64))
    else:
        w = tn.clone(w_init)
    if h_init is None:
        h = simplex_norm(tn.rand((r, m), dtype=tn.float64))
    else:
        h = tn.clone(h_init)
    if sigma_init is None:
        sigma = [i for i in range(r)]
    else:
        sigma = sigma_init.copy()
    
    total_cost = pb(w, h, sigma, data, dictionary, reg, ent, rho1, rho2,
                    cost, method=method, numItermax=numItermax, thr=thr, 
                    verbose = verbose)
    
    print("Coût initial : " + str(total_cost) + "\n")
    
    cost_list = [total_cost]
   
    current_cost = total_cost
    g1 = None
    g2 = None
    g = None
    plt.plot(w[:,0])
    plt.show()
    
    for i in range(itertotal):
        w_n, g1, g2 = optim_w(w, h, data, sigma, dictionary, reg,
                    ent, rho1, rho2, cost,
                    g1, g2,
                    step_g1, step_g2, iter_g1, iter_g2,
                    iterdual_w, method, numItermax, thr, verbose)
        
        total_cost = pb(w_n, h, sigma, data, dictionary, reg, ent, rho1, rho2,
                        cost, method=method, numItermax=numItermax, thr=thr, 
                        verbose = verbose)
        plt.plot(w_n[:,0])
        plt.show()
        
        if True : #total_cost < current_cost :
            w = w_n
            current_cost = total_cost
        
        print("Coût itération après update W " + str(i) + " : " + str(current_cost) + "\n")
        cost_list = cost_list + [current_cost]
        
        h_n, g = optim_h(w, h, data, sigma, dictionary, reg, ent, rho1, rho2, cost,
                      g, step_h, iterdual_h, method, 
                      numItermax, thr, verbose)
        
        total_cost = pb(w, h_n, sigma, data, dictionary, reg, ent, rho1, rho2,
                        cost, method=method, numItermax=numItermax, thr=thr, 
                        verbose = verbose)
        
        if True : #total_cost < current_cost :
            h = h_n
            current_cost = total_cost
        
        print("Coût itération après update H " + str(i) + " : " + str(current_cost) + "\n")
        cost_list = cost_list + [current_cost]
        
        if i%jump == 0 :
            sigma_n = update_sigma(dictionary, w, cost, ent, 
                             numItermax = numItermax, method = method)
            
            total_cost = pb(w, h, sigma_n, data, dictionary, reg, ent, rho1, rho2,
                            cost, method=method, numItermax=numItermax, thr=thr, 
                            verbose = verbose)
        
            if True : #total_cost < current_cost :
                sigma = sigma_n
                current_cost = total_cost
            
            print("Coût itération après update Sigma " + str(i) + " : " + str(current_cost) + "\n ~~~~~~~~~~~~~ \n")
            cost_list = cost_list + [current_cost]
    
    return (w, h, sigma, cost_list)